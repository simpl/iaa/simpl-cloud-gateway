# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.0] - 2024-12-02

### Added
-  Added handling for business events logging

### Changed
- private/public prefixes have been removed.
- Route GET onboarding/participant-type/* made public.

## [0.7.0] - 2024-11-11

### Added
- Added authentication provider route
- Business logging