package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;

@Log4j2
public record Rule(HttpMethod method, String path, List<String> roles) {}
