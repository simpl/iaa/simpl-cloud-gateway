package eu.europa.ec.simpl.gatewayserver.configurations;

import java.util.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class ClientCredentialsFilter extends AbstractGatewayFilterFactory<Map> {

    public static String CLIENT_ID_CLAIM = "client_id";
    private final ReactiveJwtDecoder jwtDecoder;

    @Setter
    @Getter
    @Value("${spring.security.oauth2.resourceserver.jwt.authorities-claim-name:client-roles}")
    private String roleClaim;

    public ClientCredentialsFilter(ReactiveJwtDecoder jwtDecoder) {
        super(Map.class);
        this.jwtDecoder = jwtDecoder;
    }

    @Override
    public GatewayFilter apply(Map map) {
        var clients = getConfig(map);
        return (exchange, chain) -> {
            String authHeader = exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                return unauthorized(exchange);
            }

            String token = authHeader.substring(7);

            return jwtDecoder
                    .decode(token)
                    .flatMap(jwt -> {
                        if (!isValidToken(jwt, clients)) {
                            return unauthorized(exchange);
                        }
                        return chain.filter(exchange);
                    })
                    .onErrorResume(ex -> serverError(exchange));
        };
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static List<ClientDTO> getConfig(Map map) {
        return new Config(map).clients();
    }

    private boolean isValidToken(Jwt jwt, List<ClientDTO> clients) {
        return clients.stream().anyMatch(clientFromConfig -> {
            var clientFromToken = getClientFromToken(jwt, clientFromConfig);
            return isClient(jwt, clientFromConfig) && hasRoles(clientFromConfig, clientFromToken);
        });
    }

    private boolean hasRoles(ClientDTO clientFromConfig, ClientDTO clientFromToken) {
        return new HashSet<>(clientFromToken.roles()).containsAll(clientFromConfig.roles());
    }

    private boolean isClient(Jwt jwt, ClientDTO clientFromConfig) {
        return clientFromConfig.clientId().equals(jwt.getClaimAsString(CLIENT_ID_CLAIM));
    }

    private ClientDTO getClientFromToken(Jwt jwt, ClientDTO clientFromConfig) {
        var roles = jwt.getClaimAsStringList(getRoleClaim());
        return new ClientDTO(clientFromConfig.clientId(), roles);
    }

    private Mono<Void> unauthorized(ServerWebExchange exchange) {
        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
        return exchange.getResponse().setComplete();
    }

    private Mono<Void> serverError(ServerWebExchange exchange) {
        exchange.getResponse().setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        return exchange.getResponse().setComplete();
    }

    @Getter
    @Accessors(fluent = true)
    public static class Config {
        private final List<ClientDTO> clients;

        public Config(Map<String, String> clients) {
            this.clients = clients.entrySet().stream()
                    .map(e -> new ClientDTO(
                            e.getKey(),
                            Arrays.stream(e.getValue().split(","))
                                    .filter(s -> !s.isBlank())
                                    .map(String::trim)
                                    .toList()))
                    .toList();
        }
    }

    public record ClientDTO(String clientId, List<String> roles) {
        public ClientDTO {
            Objects.requireNonNull(clientId);
            roles = List.copyOf(roles);
        }
    }
}
