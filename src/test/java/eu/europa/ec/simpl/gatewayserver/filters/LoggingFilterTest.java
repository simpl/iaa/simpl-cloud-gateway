package eu.europa.ec.simpl.gatewayserver.filters;

import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.gatewayserver.configurations.LoggingRule;
import eu.europa.ec.simpl.gatewayserver.configurations.RouteConfig;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
class LoggingFilterTest {
    @Mock
    private RouteConfig routeConfig;

    @Mock
    private RouteConfig.Logging logging;

    @Mock
    private WebFilterChain filterChain;

    @Mock
    private ServerWebExchange exchange;

    @Mock
    private ServerHttpRequest request;

    @Mock
    private LoggingRule loggingRule;

    @Mock
    private LoggingRule.Config loggingConfig;

    @Mock
    private RequestPath path;

    private LoggingFilter loggingFilter;

    @BeforeEach
    void setUp() {
        when(filterChain.filter(exchange)).thenReturn(Mono.empty());
        when(routeConfig.logging()).thenReturn(logging);
        when(logging.business()).thenReturn(List.of(loggingRule));
        when(exchange.getRequest()).thenReturn(request);
        loggingFilter = new LoggingFilter(routeConfig);

        HttpHeaders headers = new HttpHeaders();
        when(request.getHeaders()).thenReturn(headers);
        when(request.getMethod()).thenReturn(HttpMethod.GET);
        when(request.getPath()).thenReturn(path);
        when(path.value()).thenReturn("/test-path");
        when(loggingRule.config()).thenReturn(loggingConfig);
        when(loggingConfig.getMessage()).thenReturn("Log message");
    }

    @Test
    void testFilter_withMatchingLoggingRule() {
        when(loggingRule.matches(exchange)).thenReturn(Mono.just(true));

        Mono<Void> result = loggingFilter.filter(exchange, filterChain);

        StepVerifier.create(result).verifyComplete();

        verify(loggingRule, times(1)).matches(exchange);
        verify(routeConfig, times(1)).logging();
        verify(logging, times(1)).business();
    }
}
