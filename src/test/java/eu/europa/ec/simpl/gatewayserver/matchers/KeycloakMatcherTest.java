package eu.europa.ec.simpl.gatewayserver.matchers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.http.server.reactive.MockServerHttpRequest;
import org.springframework.mock.web.server.MockServerWebExchange;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher.MatchResult;
import reactor.test.StepVerifier;

@ExtendWith(MockitoExtension.class)
class KeycloakMatcherTest {
    @InjectMocks
    private KeycloakMatcher keycloakMatcher;

    @Test
    void matchesShouldReturnMatchForAuthPath() {
        var request = MockServerHttpRequest.get("/auth/login").build();
        var exchange = MockServerWebExchange.from(request);

        var result = keycloakMatcher.matches(exchange);

        StepVerifier.create(result).expectNextMatches(MatchResult::isMatch).verifyComplete();
    }
}
