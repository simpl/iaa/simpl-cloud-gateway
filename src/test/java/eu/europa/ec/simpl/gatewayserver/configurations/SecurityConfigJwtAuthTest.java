package eu.europa.ec.simpl.gatewayserver.configurations;

import static java.util.Collections.emptySet;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;

@SpringBootTest
@TestPropertySource(properties = {"gateway.url=http://localhost:8080", "keycloak.app.realm=test-realm"})
class SecurityConfigJwtAuthTest {

    @MockitoBean
    private RouteConfig routeConfig;

    @MockitoBean
    private SwaggerConfig swaggerConfig;

    @Autowired
    private SecurityConfig securityConfig;

    private ServerHttpSecurity serverHttpSecurity;

    @BeforeEach
    void setup() {
        serverHttpSecurity = ServerHttpSecurity.http();
    }

    @Test
    void testJwtAuth() {
        var publicUrls = Arrays.asList(
                new Rule(HttpMethod.GET, "/public/**", null), new Rule(HttpMethod.GET, "/api/open/**", null));

        var deniedUrls = Arrays.asList(new Rule(null, "/admin/**", null), new Rule(null, "/internal/**", null));

        var rbacRules = Arrays.asList(
                new Rule(HttpMethod.GET, "/api/user/**", Arrays.asList("USER", "ADMIN")),
                new Rule(HttpMethod.POST, "/api/admin/**", Arrays.asList("ADMIN")));

        when(swaggerConfig.urls()).thenReturn(emptySet());
        when(routeConfig.publicUrls()).thenReturn(publicUrls);
        when(routeConfig.deniedUrls()).thenReturn(deniedUrls);
        when(routeConfig.rbac()).thenReturn(rbacRules);

        SecurityWebFilterChain result = securityConfig.jwtAuth(serverHttpSecurity);

        assertNotNull(result);
    }
}
