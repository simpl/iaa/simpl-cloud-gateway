package eu.europa.ec.simpl.gatewayserver;

import static org.assertj.core.api.Assertions.assertThat;

import eu.europa.ec.simpl.gatewayserver.configurations.RouteConfig;
import eu.europa.ec.simpl.gatewayserver.configurations.SwaggerConfig;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;

@SpringBootTest()
@TestPropertySource(properties = {"gateway.url=http://localhost:8080", "keycloak.app.realm=test-realm"})
class GatewayServerApplicationTest {

    @Autowired
    private ApplicationContext applicationContext;

    @MockitoBean
    private SwaggerConfig swaggerConfig;

    @MockitoBean
    private RouteConfig routeConfig;

    @Test
    void mainMethodRuns() {
        assertThat(applicationContext).isNotNull();
    }
}
